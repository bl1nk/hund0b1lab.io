---
layout: tags
title: Archive
permalink: /archive/
---

<h1 style="padding-bottom:15px">
	Archive
</h1>

<div class="archive">
	{% for post in site.posts %}
	  {% assign currentdate = post.date | date: "%Y" %}
	  {% if currentdate != date %}
	    {% unless forloop.first %}</ul>{% endunless %}
		<h2 id="y{{post.date | date: "%Y"}}">{{ currentdate }}</h2>
		<ul>
	    {% assign date = currentdate %}
	  {% endif %}
		<li><a href="{{ post.url }}">{{ post.title }}</a>
		</li>
	  {% if forloop.last %}</ul>{% endif %}
	{% endfor %}
</div>


<h1 style="padding:5px 0">
	Tags
</h1>

{% capture site_tags %}{% for tag in site.tags %}{{ tag | first }}{% unless forloop.last %},{% endunless %}{% endfor %}{% endcapture %}

{% assign tag_words = site_tags | split:',' | sort %}

<ul class="tags-cloud">
    {% for item in (0..site.tags.size) %}{% unless forloop.last %}
        {% capture this_word %}{{ tag_words[item] }}{% endcapture %}
            <li>
                <a href="#{{ this_word | cgi_escape }}" class="tag">{{ this_word }}
                    <span>({{ site.tags[this_word].size }})</span>
                </a>
            </li>
        {% endunless %}
    {% endfor %}
</ul>

<div class="archive">
    {% for item in (0..site.tags.size) %}{% unless forloop.last %}
    {% capture this_word %}{{ tag_words[item] }}{% endcapture %}
	<h3 id="{{ this_word | cgi_escape }}">{{ this_word }}</h3>
		<ul>
		{% for post in site.tags[this_word] %}{% if post.title != null %}
			<li>
				<a href="{{ post.url | prepend: site.baseurl }}" >{{ post.title }}</a>
			</li>
		{% endif %}{% endfor %}
		</ul>
	{% endunless %}{% endfor %}
</div>
