---
layout: post
title: Use a pixelated version of your desktop as your lockscreen with i3lock
tags:
  - i3lock
  - i3
  - Scrot
  - ImageMagick
---

I decided it was time to spice up my lockscreen, I've been using the same old static picture for what feels like years now. 

I've seen a lot of people use blurred and pixelated screenshots of their desktop and they do look pretty cool, so I decided to do the same thing myself. :)

[![My lockscreen using i3lock and a pixelated screenshot of my desktop](/images/thumbnails/i3lockScrot.png)](/images/i3lockScrot.png)

I wrote a Bash script that uses `scrot` and `imagemagick` to take a screenshot of my desktop, pixelate the image, add the lock icon to it and lastly lock the screen using the newly created image.

```bash
#!/bin/bash

icon="$HOME/.config/i3/lock.png"
img="$HOME/.cache/i3lock.png"

scrot $img
# Pixelate image
convert $img -scale 10% -scale 1000% $img
# Blur image
#convert $img -blur 0x4 500% $img
convert $img $icon -gravity center -composite $img
i3lock -u -i $img
```

The lock icon I use can be found on the website Iconfinder.com [here](https://www.iconfinder.com/icons/1891028/lock_red_icon) and I used the 256px version myself.

I also included the option to blur the image, which does look cool as well, but that operation is noticeably slower than pixelating the image.
