---
layout: post
title: Using i3lock as a Systemd service
tags:
  - i3
  - i3lock
  - Systemd
  - Suspend
---

It took me a few tries before figuring this one out. But I eventually found the solution in an old thread on the Arch Linux forums [[Link](https://bbs.archlinux.org/viewtopic.php?pid=1170536#p1170536)].

I created the file `i3lock.service` with the following content:

```
[Unit]
Description=Lock screen before suspend
Before=sleep.target

[Service]
User=<user>
Type=forking
Environment=DISPLAY=:0
ExecStart=/home/johan/Scripts/i3lock.sh

[Install]
WantedBy=sleep.target
```

I then copied the file to the folder `/etc/systemd/system/` and enabled it with the command:

```
# systemctl enable i3lock.service
```

And my `i3lock.sh` looks like this:

```bash
#!/bin/bash

icon="$HOME/.config/i3/lock.png"
img="$HOME/.cache/i3lock.png"

scrot $img
# Pixelate image
convert $img -scale 10% -scale 1000% $img
# Blur image
#convert $img -blur 0x4 500% $img
convert $img $icon -gravity center -composite $img
i3lock -u -i $img
```

It will not lock my screen before it goes to sleep, so it's locked when it wakes up again.
