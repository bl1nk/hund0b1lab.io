---
layout: post
title: toot - A Mastodon CLI client
tags:
 - toot
 - Mastodon
 - CLI
 - Applications
---

My main Mastodon client is Bitlbee with the plugin [bitlbee-mastodon](https://github.com/kensanata/bitlbee-mastodon), it's nice having both IRC, XMPP and Mastodon under the same roof. But it only comes with the very basic features; reading, replying, liking and boosting posts. Which is for the most part enough for me, but I sometimes post images and maybe want to check out a profile.

This is where [toot](https://github.com/ihabunek/toot) comes into the picture. toot is a Mastodon client for the command-line interface and it's packed with a lot of features that bitlbee-mastodon doesn't have.

The command `toot` will list all the commands available for you:

```
$ toot
toot - a Mastodon CLI client
v0.18.0

Authentication:
  toot login           Log in from the console, does NOT support two factor authentication
  toot login_browser   Log in using your browser, supports regular and two factor authentication
  toot activate        Switch between logged in accounts.
  toot logout          Log out, delete stored access keys
  toot auth            Show logged in accounts and instances

Read:
  toot whoami          Display logged in user details
  toot whois           Display account details
  toot instance        Display instance details
  toot search          Search for users or hashtags
  toot timeline        Show recent items in your public timeline
  toot curses          An experimental timeline app (doesn't work on Windows)

Post:
  toot post            Post a status text to your timeline
  toot upload          Upload an image or video file

Accounts:
  toot follow          Follow an account
  toot unfollow        Unfollow an account
  toot mute            Mute an account
  toot unmute          Unmute an account
  toot block           Block an account
  toot unblock         Unblock an account

To get help for each command run:
  toot <command> --help
```

It's fairly straight forward and if you want to know more about a specific command you can show more information by passing the flag `--help`:

```
$ toot post --help
usage: toot post [-h] [-m MEDIA] [-v VISIBILITY] [--no-color] [--debug]
                 [-u USING]
                 [text]

Post a status text to your timeline

positional arguments:
  text                  The status text to post.

optional arguments:
  -h, --help            show this help message and exit
  -m MEDIA, --media MEDIA
                        path to the media file to attach
  -v VISIBILITY, --visibility VISIBILITY
                        post visibility, one of: public, unlisted, private,
                        direct
  --no-color            don't use ANSI colors in output
  --debug               show debug log in console
  -u USING, --using USING
                        the account to use, overrides active account

https://github.com/ihabunek/toot
```

Here's an example of what it looks like when you show your timelime:

```
$ toot timeline
───────────────────────────────┼────────────────────────────────────────────────────────────────────────────────────────
F-Droid                        │ Aurora Store: Download apks from #GooglePlayStore#AuroraStore is a #fork of
@fdroidorg                     │ #YalpStore by Sergey Yeriomin. It lets you download apps directly from Google
2018-06-17 10:29               │ Play Store as #apk files. It can search for #updates of installed #apps and lets
Reblogged @Aznorth             │ you search for other apps.Aurora Store provides a #MaterialUI to #Yalp
                               │ #Store.https://f-droid.org/app/com.dragons.aurora#fdroidorg #fdroid #libre #app
                               │ @fdroidorg #altenative
───────────────────────────────┼────────────────────────────────────────────────────────────────────────────────────────
F-Droid                        │ This weeks #twif is live:
@fdroidorg                     │ https://f-droid.org/en/2018/06/14/this-week-in-fdroid-w24.htmlEnjoy!
2018-06-17 08:23               │ 
───────────────────────────────┼────────────────────────────────────────────────────────────────────────────────────────
```
If you want to list the public timeline just add the flag `--public` and to list the timeline for you instance only you add the flags `--public --local`.

And here's what it looks like when you do a search and look up a profile:

```
$ toot search pixelfed

Accounts:
* @pixelfed@mastodon.social PixelFed

Hashtags:
#pixelfed, #pixelfedlogo, #pixelfed_replaces_instagaram, #pixelfed_replaces_instagram
```

```
$ toot whois @pixelfed@mastodon.social

@pixelfed@mastodon.social PixelFed

Federated Image Sharing powered by ActivityPub.Created by @dansupPress
Inquiries: hello@pixelfed.org

ID: 50992
Since: 2018-05-17 @ 21:27:57

Followers: 1239
Following: 0
Statuses: 249

https://mastodon.social/@pixelfed
```

And why not follow them so we can monitor the progress of the awesome project [PixelFed](https://pixelfed.social)? :)

```
$ toot follow @pixelfed@mastodon.social
✓ You are now following @pixelfed@mastodon.social
```

That's about it. toot is a great little tool if you prefer doing things in the shell like me.

# Installation

You need the dependencies `python-requests`, `python-beautifulsoup4` and the make dependency `python-setuptools`. You can then download the latest version of toot on GitHub [here](https://github.com/ihabunek/toot/releases).

Unpack the archive and simply install it with the command `python setup.py install`.

# Configuration

Before you can use toot you need to add your account(s). You can do so by running the command `toot login` or `toot login_browser` if you're using two factor authentication, you will be then directed to your Mastodon instance via your web browser and then asked to authorize toot.

toot supports multiple accounts at the same time, you just have to repeat what you just did for your other instance and you can then view your logged in accounts with the command `toot auth`. The active account will have the flag `ACTIVE` next to it. You can then use the command `toot activate` to switch between your accounts, or just use the flag `--using` in combination with your command to use an account temporarily.

If you wish to delete your access token you can do so by logging out using the command `toot logoout`.
