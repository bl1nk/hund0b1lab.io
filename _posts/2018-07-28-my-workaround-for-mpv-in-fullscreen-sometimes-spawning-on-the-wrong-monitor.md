---
layout: post
title: My workaround for mpv in fullscreen sometimes spawning on the wrong monitor
tags:
  - i3
  - mpv
---

When I run mpv with the flag `--fullscreen` in my window manager i3, the client sometimes spawns on the wrong monitor. The reason for this behaviour is due to the fact that i3 doesn't support the EMMH hint called "[_NET_WM_FULLSCREEN_MONITORS](https://specifications.freedesktop.org/wm-spec/wm-spec-latest.html#idm140200472531472)", which is something you can read more about on their issue tracker [here](https://github.com/i3/i3/issues/2116).

But as a workaround for this issue I created a rule for mpv in i3 that only gets called when I run mpv with the flag `--title mvpFullscreen`:

```
for_window [title="mpvFullscreen"] border none, floating toggle, move window to position 3840 0, resize set 1920 1080
```

The rule makes mpv float, removes the decoration, moves it to the upper left corner of my third monitor and then rezies it to the same size as my monitor. It's a fake but instant and flawless looking fullscreen and I'm really happy with it.
