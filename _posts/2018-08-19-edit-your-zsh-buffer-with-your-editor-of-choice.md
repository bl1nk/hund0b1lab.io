---
layout: post
title: Edit the zsh buffer with your $EDITOR of choice
tags:
  - zsh
  - Vim
---

If you spend a lot of time in the shell you also probably edit a lot of commands, which at times can be tedious when they're getting a bit to long and complex. Being able to easily fire up your `$EDITOR` of choice when needed can therefore be a real blessing.

To make it work you need to add these lines to your configuration file `~/.zshrc`:

```
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^I' edit-command-line
```

And don't forget to set the `$EDITOR` variable as well:

```
export EDITOR="vim"
```

I have it bound to `Ctrl+I` to be consistent with how I open the `$EDITOR` in qutebrowser, but you're free to bind it to whatever makes sense for you. And if you use the Vi-mode in zsh like me it's probably a good idea to bind a key for it in the `normal` mode as well:

```
bindkey -M vicmd '^I' edit-command-line
```

Save your changes and source your configuration with the command `source ~/.zshrc` or just restart the shell for it to take effect.
