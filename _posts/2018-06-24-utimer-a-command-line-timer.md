---
layout: post
title: µTimer - A command-line timer
tags:
  - uTimer
  - CLI
  - Applications
---

[µTimer](https://launchpad.net/utimer) is a neat little command-line tool that features a timer, countdown and stopwatch.

It's very straight forward, to set a timer you use the flag `-t` and to set a countdown you use the flag `-c` with the time like `1d2h3m`, `5m30s` or just `5m`:

```
$ utimer -c 5m30s
Time Remaining: 0 days 00:05:26.358 (326.358 seconds)
```

```
$ utimer -t 5m
Elapsed Time: 0 days 00:00:04.353 (4.353 seconds)
```

You can use the following in any combination for the timer and countdown:

| d | days |
| h | hours |
| m | minutes |
| s | seconds |
| ms | milliseconds |

And to start a stopwatch:

```
$ utimer -s
Elapsed Time: 0 days 00:00:03.482 (3.482 seconds)
```

To stop the stopwatch you use the key `q`. And for more information you can always consult the flag `--help`.

# Installation

If you have access to the Arch User Repo you can install it via there, the name of the package is [utimer](https://aur.archlinux.org/packages/utimer/). But if you're using something else you probably need to compile it yourself.

I tried compiling it in my virtual machine with Ubuntu 16.04, so I could verify the instructions and add them here, but it didn't go as planned. When I ran `./configure` it complained about two packages:

> checking for GLIB... configure: error: Package requirements (glib-2.0 >= 2.16.6 gobject-2.0) were not met:
>
>No package 'glib-2.0' found
>No package 'gobject-2.0' found

I started with the first dependency by installing the package `libglib-2.0-dev` using `apt`:

```
# apt install libglib2.0-dev
[sudo] password for johan:
Reading package lists... Done
Building dependency tree
Reading state information... Done
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies.
 libglib2.0-dev : Depends: libpcre3-dev (>= 1:8.31) but it is not going to be installed
E: Unable to correct problems, you have held broken packages.
```

I have held broken packages? Let's check them out:

```
$ apt-mark showhold
$
```

OK? Am I doing something wrong here or what? Anyway I also tried my luck with `aptitude`: 

```
# aptitude install libglib2.0-dev
The following NEW packages will be installed:
  libglib2.0-bin{a} libglib2.0-dev libpcre3-dev{ab} libpcre32-3{a} zlib1g-dev{a}
0 packages upgraded, 5 newly installed, 0 to remove and 0 not upgraded.
Need to get 2,246 kB of archives. After unpacking 12.6 MB will be used.
The following packages have unmet dependencies:
 libpcre3-dev : Depends: libpcre3 (= 2:8.38-3.1) but 2:8.41-1.1+ubuntu14.04.1+deb.sury.org+4 is installed.
The following actions will resolve these dependencies:

     Keep the following packages at their current version:
1)     libglib2.0-dev [Not Installed]
2)     libpcre3-dev [Not Installed]

Accept this solution? [Y/n/q/?] y
No packages will be installed, upgraded, or removed.
0 packages upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Need to get 0 B of archives. After unpacking 0 B will be used.
```

If anyone is reading this and is actually using Debian (or anything based on it) and knows how to fix this, please let me know via any of the links in the footer.
