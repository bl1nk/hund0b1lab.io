---
layout: post
title: Adding line numbers to a list in Vim with awk
tags:
  - Vim
---

I have finally found a good way of adding line numbers to a list in Vim. Most tips I've found so far has been overly complicated and with various success, this option uses `awk` and it's in return a bit simpler.

```
:'<,'>%!awk '{print NR".",$0}'
```

It works by you selecting the lines with `Ctrl+v` and then execute the command, it will then transform your list from this:

```
One
Two
Three
```

To this

```
1. One
2. Two
3. Three
```

And because I don't want to manually type it every time, I decided to map it to `\n`:

```
map \n :'<,'>%!awk '{print NR".",$0}'<CR>
```

<p style="text-align:right">
	Source: <a href="https://stackoverflow.com/a/252774">Stackoverflow</a>
</p>
