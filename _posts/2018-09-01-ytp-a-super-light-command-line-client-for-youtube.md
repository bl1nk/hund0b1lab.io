---
layout: post
title: ytp - A super light command-line client for YouTube
tags:
  - ytp
  - YouTube
---

I've been using [youtube-viwer](https://github.com/trizen/youtube-viewer) for a long time now and it's a great command-line client for YouTube. It's packed with features that allow you to use pretty much every feature possible on YouTube, but it comes at a price; the footprint. 

On Gentoo Linux I needed to emerge a total of 42 packages for youtube-viewer, most are small in size, but still.. 42 packages. And I don't even use 99% of the features anyway, I just want to be able to search and play videos without having to use YouTube's hideously bloated and malicious website which tracks my every move.

But I recently found out about a super light and very basic client called [ytp](https://gitlab.com/uoou/ytp) via [HexDSL on Mastodon](https://linuxrocks.online/@HexDSL/100613936255793565). It's written in Bash and the only dependency it needs is [jq](https://stedolan.github.io/jq/), which is a lightweight command-line JSON processor.

![A screenshot of ytp in action - A super light command-line client for YouTube](/images/ytp.png)

# Installation

The only dependency you need to install is `jq` and after that you can clone the repository with git, make the script executable and run it:

```
$ git clone https://gitlab.com/uoou/ytp
```
```
$ cd ytp && chmox +x ytp
```
```
$ ./ytp "<search word>"
```

And as always, I advise you to put the script in your `$PATH` for easy access.
