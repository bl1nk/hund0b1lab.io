---
layout: post
title: Launching MultiMC instances via Rofi
tags:
  - Rofi
  - MultiMC
  - Minecraft
---

I play a lot of Minecraft, both vanilla and with mods, which is partly why I use the third party and open source launcher [MultiMC](https://multimc.org). It's a great launcher that lets you create various instances of Minecraft completely separated from each other in sandboxes.

And since they allow you to launch an instances via the commandline, why not make it work with Rofi so I don't have to launch the launcher itself and then choose which instance I want to play. :)

![A demo of the Rofi script that lets you launch MultiMC instances](/images/rofi-multimc.png)

This script is a modified version of my script for VirtualBox, which I'm not the author of, I'm not sure where I found it either.

```bash
#!/bin/bash

path="/home/$( whoami )/.multimc"

handle_selection() {
if [[ $1 ]]; then
	if [[ -d "$path/instances/$1" ]]; then
		touch "$path/instances/$1"
		notify-send "MultiMC" "Launching instance $1."
		sh $path/MultiMC -l "$1" &
    else
		 notify-send "MultiMC" "No instance called $1."
    fi
  fi
}

handle_selection "$( cd "$path/instances"; ls -t -d * | sed '/_MMC_TEMP/d;/instgroups.json/d' | rofi -font "xos4terminus 12" -m -0 -dmenu -i -p 'MultiMC' -hlbg '#5FA5D7' -bc '#5FA5D7' )"
```

I have the script in the folder `$HOME/Scripts/rofi` and I launch the Rofi menu with all the scripts by calling this script using my window manager:

```bash
#!/bin/bash
cd $HOME/Scripts/rofi; menu=$(find -L . -printf '%P\n' | sort | tail -n +2 | rofi -font "xos4terminus 12" -m -0 -bw 3 -dmenu -i -p "Rofi Scripts: "); ./"${menu}"
```
