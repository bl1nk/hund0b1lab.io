---
layout: post
title: Cleaning up filenames with detox
tags:
 - detox
 - CLI
 - Utilities
---

[Detox](http://detox.sourceforge.net) is a neat little command-line tool that helps you clean up filenames. It will either remove or replace weird character, like `Á` will be replaced with `A`, an empty space with `_` and it's even smart enough to replace CGI escaped ASCII characters like `%20` with `_`.

Here's a small example:
 
```bash
$ cd Pictues/Wallpapers/Space
$ detox --dry-run *
atlantis_nebula__7_by_starkiteckt-1080p.jpg -> atlantis_nebula_7_by_starkiteckt-1080p.jpg
cold_embrace___high_resolution___by_starkiteckt-db385qy.png -> cold_embrace_high_resolution_by_starkiteckt-db385qy.png
Hyper Cradle 1920x1080 HD.png -> Hyper_Cradle_1920x1080_HD.png
```

The defaults should be good enough for most, but there's a lot of configuration options available and you can read all about them via the manual pages.
