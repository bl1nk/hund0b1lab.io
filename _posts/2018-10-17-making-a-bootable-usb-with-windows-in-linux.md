---
layout: post
title: Making a bootable USB with Windows in Linux using WoeUSB
tags:
  - Windows
  - WoeUSB
---

[WoeUSB](https://github.com/slacka/WoeUSB) is a tool that lets you /very easily/ create a bootable USB with Windows in Linux. It works with only one command, so there's not much to write about.

```
# woeusb --device windows.iso /dev/sdX
```

There's also a GUI for it available via the command `woeusbgui`, but we all know what I think about GUIs. :)

# Installation

### Arch Linux

It's available via AUR as [woeusb](https://aur.archlinux.org/packages/woeusb/) and [woeusb-git](https://aur.archlinux.org/packages/woeusb-git/).

### Ubuntu

It's available via [WebUpd8's PPA](https://launchpad.net/~nilarimogard/+archive/ubuntu/webupd8).
