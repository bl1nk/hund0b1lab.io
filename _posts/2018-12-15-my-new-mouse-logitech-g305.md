---
layout: post
title: My new mouse - Logitech G305
tags:
  - Pointing_devices
  - Logitech
---

I don't really do reviews, this is just my 5 cents about it after owning it for more than a month. If you want an actual review from someone who actually knows computer mouses, check out Rocket Jump Ninja on YouTube. [His review about this mouse](https://www.youtube.com/watch?v=20ZXAEiiKMU) is both informative and good. :)

[![](/images/thumbnails/logitech_g305.jpg)](/images/logitech_g305.jpg)

After more than 6 years of service, my Steelseries Xai has now finally been retired. It has been slowly giving up on me for probably a year or so, with random disconnects and single clicks registering as double clicks getting more and more common.

Since I don't use the mouse that much (basically only GIMP and Minecraft), cheap me had a hard time opening up the wallet for a new one. But after several months of research and a lot of hesitation caused by said cheapness, I finally bought the decently priced [Logitech G305](https://www.logitechg.com/en-gb/products/gaming-mice/g305-lightspeed-wireless-gaming-mouse.html).

It's as you can see a wireless mouse with a small footprint. It features the popular Hero sensor that's supposedly perform good in games, but more importantly (at least for me) supposed to be energy efficient and they say that the battery will last for 9 month if you use the endurance mode instead of the gaming mode. With the gaming mode the battery should last about 250 hours, which is still good.

Even though I have large hands, I still prefer small and light mouses. This is both small (but not too small) and decently light with its 97 grams.

Speaking of weight, a positive thing with this mouse (and the only reason I even considered it in the first place) is that it supports regular sized [AA-batteries](https://en.wikipedia.org/wiki/AA_battery), which is both easy and cheap to replace. I refuse to buy a mouse with a proprietary sized rechargeable battery that will go bad in a few years.

And if 97 grams is too heavy, you can make it about 7-8 grams lighter by buying a slightly more expensive lithium battery, which is also more durable.

The mouse buttons feels way more tactile than on my old Xai, but they're also a bit louder. At first I found it annoying, but after just a few days of usage I stopped noticing it. The side buttons is a bit mushy thogh. I didn't like them at all in the beginning, but now I'm used to them and it doesn't bother me at all.

Overall I'm really happy with it and if you're looking for a new mouse I would highly looking into this mouse. :)
