---
layout: post
title: Trash-cli - A command-line interface to the FreeDesktop.org Trash
tags:
  - Trash
  - CLI
---

So, I'm sure I'm not the only user who has in the past mistakenly deleted the wrong file(s) in the shell. And it always seems to be that one super important document you just spent like five sweaty hours on as well. :/

This is where [trash-cli](https://github.com/andreafrancia/trash-cli) comes into the picture. It's a simple and straightforward command-line tool written in Python that works as a interface to the [FreeDesktop.org trash](https://specifications.freedesktop.org/trash-spec/trashspec-latest.html). And instead of straight up deleting your files with the command `rm`, you can use `trash` to put them in your FreeDesktop.org trash and then recycle them at a later stage.

# Usage

You put a file or folder in the trash with the command `trash-put <file>` or just `trash <file`. To list your trashed files you use the command `trash-list`:

```
$ trash-list
2018-07-09 14:14:04 /home/johan/Unpack-4461
2018-07-09 14:13:46 /home/johan/rsync-bosco-2018-06-17.log
2018-07-09 14:13:17 /home/johan/lol_felt_bad.jpg
```

To restore a file or a folder you use the command `trash-restore` followed by the corresponding number:

```
$ trash-restore
   1 2018-07-09 14:14:04 /home/johan/Unpack-4461
   2 2018-07-09 14:13:46 /home/johan/rsync-bosco-2018-06-17.log
   3 2018-07-09 14:13:17 /home/johan/lol_felt_bad.jpg
What file to restore [0..3]: 3
```

If you wish to recycle all your trashed files you can do so with the command `trash-empty`. Note that it's also possible to only recycle files older than a specified value, for an example this example will only recycle files older than 30 days:

```
trash-empty 30
```

# Installation

It's available in both the official repositories for Arch Linux, Ubuntu and via the Python package manager pip with the name `trash-cli`.

**Arch Linux:**

```
# pacman -S trash-cli
```

**Ubuntu:**

```
# apt install trash-cli
```

**pip:**

```
$ pip install trash-cli --user
```
