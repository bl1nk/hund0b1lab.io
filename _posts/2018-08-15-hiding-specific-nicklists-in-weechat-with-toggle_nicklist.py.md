---
layout: post
title: Hiding specific nicklists in WeeChat with toggle_nicklist.py
tags:
  - WeeChat
---

I use Mastodon via WeeChat and Bitlbee, this means that I also get a nicklist with a lot of users from Mastodon, which is not very useful and it only takes up a lot of valuable screen space.

Thankfully it's very easy to disable the nicklist for specific buffers via the script [toggle_nicklist.py](https://weechat.org/scripts/source/toggle_nicklist.py.html/).

# Installation

Installing a script is very easy in WeeChat:

```
/script install toggle_nicklist.py
```

# Usage

You use the plugin with the command `/toggle_nicklist <option>`. The options you have available is `show`, `hide`, `toggle`, `add` and `remove`.

And to hide a nicklist for a certain buffer you just jump to the buffer and then run the command `/toggle_nicklist add`. And don't forget to save any changes with `/save` when you're done.
