---
layout: post
title: Adding a pause indicator to mpv media player
tags:
  - mpv
---

Depending on what you're watching, it's not always that obvious when you pause a video in mpv. I also often tend to pause the video by mistake and confuse myself even further, so I looked it up and found a script called [pause-indicator.lua](https://gist.github.com/torque/9dbc69543118347d2e5f43239a7e609a) that adds just that.

[![A screenshot of mpv with the pause indicator visible](/images/thumbnails/mpv-pause-indicator.jpg)](/images/mpv-pause-indicator.jpg)

{:.image-caption}
*It flashes a pause icon when paused and a play icon when resumed*

# Installation

Create the folder `~/.config/mpv/scripts/` if you don't already have it:

```
$ mkdir ~/.config/mpv/scripts/
```

Then download the script to that folder:

```
$ wget https://gist.githubusercontent.com/torque/9dbc69543118347d2e5f43239a7e609a/raw/bd9fcfe68a4f13b655c686e5790cbd2ee9489475/pause-indicator.lua -P ~/.config/mpv/scripts/
```

And that's it. :)

# Configuration

I didn't like the default colours, the size of the icon and the animation duration was a bit too quick for me so I changed it. There's no configuration file, but changing the values is easy as long as you know which values it is.

**Foreground colour**

I changed the default value from `FC799E` to `E6E6E6`.

```
settings['pause-indicator-foreground-style'] = [[\c&HFC799E&]]
```

**Background colour**

I changed the default value from `2D2D2D` to `1111111`.

```
settings['pause-indicator-background-style'] = [[\c&H2D2D2D&]]
```
**Animation duration**

I changed the default value from `0.25` to `0.50`.

```
settings['animation-duration'] = 0.25
```

**Icon size**

I changed the default value from `1` to `2`.

```
settings['display-scale-factor'] = 1
```

And the result is what you can see in the image above.

