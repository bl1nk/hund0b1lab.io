---
layout: post
title: sxiv - A simple Vi-like image viewer
tags:
 - sxiv
 - Image_viewer
 - Applications
---

[sxiv](https://github.com/muennich/sxiv/) is a fast Vi-like image viewer with a [suckless](https://suckless.org/) approach to it. It's fairly lightweight in terms on its footprint, but it still has everything that you can expect from an image viewer and on top of that it's also scriptable.

<center>
	<a href="/images/sxiv.jpg"><img src="/images/sxiv-thumb.jpg" style="display:unset;margin:1em 0"></a>
	<a href="/images/sxiv-thumbs.jpg"><img src="/images/sxiv-thumbs-thumb.jpg" style="display:unset;margin:1em 0"></a>
</center>

And when I say it's fast I really mean it! Loading huge images with a file size at 50 MB takes 1-2 seconds on my 8 year old computer, while regular sized images are instant.

The website lists these features:

* Basic image operations, e.g. zooming, panning, rotating
* Customizable key and mouse button mappings (in config.h)
* Thumbnail mode: grid of selectable previews of all images
* Ability to cache thumbnails for fast re-loading
* Basic support for multi-frame images
* Load all frames from GIF files and play GIF animations
* Display image information in status bar

The second point is one of the few things I dislike with the suckles-philosophy though. They don't use configuration files, which means that if you want to change any of the settings you need to edit the source code. It's for the most part straight forward, but it also means that you need to download the source and compile it yourself. I would prefer if I could just create a configuration file in my folder `$CONFIG`, that I can also easily share with my friends and/or across my own computers with my other [dotfiles](https://gitlab.com/Hund0b1/dotfiles). With that said, it's still a great image viewer and you're most likely not going to need to change any of the settings anyway. 

# Using sxiv

To use sxiv you either open an image directly with `sxiv <file>` or by open a folder in thumbnail mode: `xsiv -tr <folder>` or `xsiv -tr {<folder>,<second folder>}` for multiple folders. The flag `-r` tells it to search the given directory (or directories) recursively for images to view and the flag `-t` tells it to start in thumbnail mode.

There's a lot of shortcuts for sxiv -- but they're all listed in the [man page for sxiv](http://muennich.github.io/sxiv/sxiv.1.html) -- so I'm only going to mention the most common ones to get you started:

<ul>
<li> n - Go to the next image.</li>
<li> p - Go to the previous image.</li>
<li> g - Jump to the first image.</li>
<li> G - Jump to the last image.</li>
<li> f - Toggle fullscreen mode.</li>
<li> b - Toggle the bar.</li>
<li> + - Zoom in.</li>
<li> - - Zoom out.</li>
<li> e - Fit image to the window width.</li>
<li> E - Fit image to the window height.</li>
<li> q - Exit sxiv.</li>
</ul>

# Make sxiv show the image size and file size in the bar

For it to show the image size and file size you need to install the packages `exiv2` and `imagemagick`, then create the file `~/.config/sxiv/exec/image-info` with this content:

```bash
#!/bin/sh

# Example for ~/.config/sxiv/exec/image-info
# Called by sxiv(1) whenever an image gets loaded,
# with the name of the image file as its first argument.
# The output is displayed in sxiv's status bar.

s=" | " # field separator

filename=$(basename "$1")
filesize=$(du -Hh "$1" | cut -f 1)

# The '[0]' stands for the first frame of a multi-frame file, e.g. gif.
geometry=$(identify -format '%wx%h' "$1[0]")

tags=$(exiv2 -q pr -pi "$1" | awk '$1~"Keywords" { printf("%s,", $4); }')
tags=${tags%,}

echo "${filesize}${s}${geometry}${tags:+$s}${tags}${s}${filename}"
```

Source: [ArchWiki](https://wiki.archlinux.org/index.php/Sxiv#Showing_the_image_size_in_the_status_bar).

# Adding shortcuts to sxiv

It's also possible to add shortcuts to sxiv that let's you execute any shell command you like. Let's start with creating the file `$CONFIG/.sxiv/exec/key-handler and adding this example content to it:

```sh
#!/bin/sh
while read file
do
	case "$1" in
	"C-u")
		exiftool -all= "$file" && shareLinkCreator "$file" ;;
	"C-w")
		feh --bg-fill "$file" ;;
	"C-d")
		trash-put "$file" ;;
	esac
done
```

This is the shortcuts I use and it works by first pressing `Ctrl+x` and then the shortcut, like `Ctrl+d` to move the selected file to my trash.
