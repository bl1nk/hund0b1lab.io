---
layout: post
title: XMPP via the command-line with sendxmpp
tags:
  - XMPP
  - sendxmpp
  - sendmail
  - Command-line
---

[sendxmpp](https://sendxmpp.hostname.sk/) is a XMPP client for the command-line. It's basically a Perl-script and it's intended to be an alternative to [sendmail](https://en.wikipedia.org/wiki/Sendmail).

I use it for my servers to notify me about various events that you would normally use something like sendmail for. Two examples is that rTorrent sends me message when it has finished downloading a new torrent, which is handy since it automatically downloads torrents from a local folder that I share via the network and I don't always have the session with rTorrent open to check its progress.

The other example is that it send the regular reports from [Logwatch](https://wiki.archlinux.org/index.php/Logwatch) via XMPP, which is something I used to do via sendmail.

And here's how it works:

#### Arch Linux:

```bash
$ echo "Hello world" | /usr/bin/site_perl/sendxmpp --tls-ca-path="/etc/ssl/certs" -t hund@example.tld
```

#### Debian:

```bash
$ echo "Hello world" | /usr/bin/sendxmpp --tls-ca-path="/etc/ssl/certs" -t hund@example.tld
```
I had issues with some Bash script when I didn't specify the static path to be binary on both my Arch Linux and Debian machines, which is why I use the full path. I also had to use the flag `--tls-ca-path="/etc/ssl/certs"` for it to work with TLS.

### The rTorrent script

The script for rTorrent looks like this:

```bash
#!/bin/bash
echo -e "New torrent!\n\n$1" | /usr/bin/sendxmpp --tls-ca-path="/etc/ssl/certs" -t hund@example.tld
```

And in my `$HOME/.rtorrent.rc` I added this:

```
method.set_key = event.download.finished,notify_xmpp,"execute2=/home/johan/Scripts/rtorrent-xmpp.sh,$d.name="
```


# Installation

It's available in the official repositories for Debian and Gentoo Linux as `sendxmpp` and `net-im/sendxmpp`. If you're running Arch Linux you need to turn to the AUR; [sendxmpp](https://aur.archlinux.org/packages/sendxmpp/) and [sendxmpp-git](https://aur.archlinux.org/packages/sendxmpp-git/).

# Configuration

The configuration file is `$HOME/.sendxmpprc` and my configurations looks like this:

```
username: hund
jserver: example.tld:5222
component: example.tld
password: ***
```

And don't forget to set some stricter permissions for the configuration:

```
$ chmod 600 ~/.sendxmpprc
```

That should be all. :)

# Credits

A big thanks to [vanad.is](https://vanad.is) and the blog post "[Send system cronjob output via XMPP](https://vanad.is/send-system-cronjob-output-via-xmpp.html)". It's where I found out about this neat client.
