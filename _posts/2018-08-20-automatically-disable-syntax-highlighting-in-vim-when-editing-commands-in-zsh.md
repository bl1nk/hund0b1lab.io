---
layout: post
title: Automatically disable syntax highlighting in Vim when editing the zsh buffer
tags:
  - zsh
  - Vim
---

I often [edit my zsh buffer in Vim]({{ site.url }}/2018/08/19/edit-your-zsh-buffer-with-your-editor-of-choice.html) and one of the reasons for it is to have access to spell check (using [GNU Aspell](http://aspell.net)).

Spell check is a very nice feature to have when I do things like posting an update to Mastodon via the command-line client [toot](https://github.com/ihabunek/toot). But one annoying thing with it is that the syntax highlighting feature in Vim overrides the misspelled words, making it impossible to spot potentially misspelled words.

![Vim with syntax highlighting enable and disabled for the zsh buffer](/images/vim-zsh-syntax-highlighting.png)
*Vim with syntax highlighting enabled and disabled for the zsh buffer.*

Thankfully there's an easy way of automatically disabling the syntax highlighting feature in Vim when editing the zsh buffer, you just add one tiny line to your Vim configuration:

```
autocmd FileType zsh set syntax=off
```

Save and reload your configuration and you're set.
