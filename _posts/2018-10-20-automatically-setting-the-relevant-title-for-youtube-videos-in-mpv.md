---
layout: post
title: Automatically setting the relevant title for YouTube videos in mpv
tags:
  - mpv
  - YouTube
---

I was annoyed with the static title "mpv" when I watch YouTube videos via mpv, so I made this little script that automatically sets the relevant title in mpv for me.

```bash
#!/bin/bash

title=$(curl -Ss $1 | grep -oE "<title>.*</title>" | sed 's/<title>//;s/- YouTube<\/title>//')
mpv $1 --ytdl --title "mpv -- $title"
```

I named it `p.sh` and you call it with `p.sh <url>`.
