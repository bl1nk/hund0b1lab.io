---
layout: post
title: Grouping and sorting regular and symlinked folders before files with ls in zsh
tags:
  - zsh
---

By default `ls` sorts all folders, symlinked folders and files by name and not by type and then by name, which is very annoying and sometimes even confusing.

Thankfully you can change this behavior pretty easy, you just need to add one small function to your zsh configuration:

```zsh
dir1st() { [[ -d $REPLY ]] && REPLY=1-$REPLY || REPLY=2-$REPLY;}
```

Then call it like this:

```zsh
ls -ldU -- *(o+dir1st)
```
And here's an example image:

![](/images/lsorder.png)

If you're a Bash user and knows how to make this work there, let me know! I'm pretty sure there's a lot of Bash users out there who would want this as well.
