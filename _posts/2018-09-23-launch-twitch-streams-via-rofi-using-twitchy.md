---
layout: post
title: Launch Twitch streams via Rofi using Twitchy
tags:
  - Rofi
  - Twitchy
  - Twitch
---

I wrote this little script for Rofi that lets me launch Twitch streams using Twitchy. It requires [Rofi](https://github.com/DaveDavenport/rofi), [Twitchy](https://github.com/BasioMeusPuga/twitchy) and [Streamlink](https://github.com/streamlink/streamlink) to work.

![My script for Rofi that lets me launch Twitch streams using Twitchy](/images/rofi-twitchy.png)

```bash
#!/bin/bash

handle_selection() {
	if [[ $1 ]]; then
		name=$(echo $1 | awk {'print $1'})
		notify-send "Twitchy" "Launching the stream with $name"
		streamlink https://twitch.tv/$name best
	else exit 1
	fi
}

handle_selection "$( twitchy --non-interactive | sort | awk -F, 'BEGIN{OFS=" - ";} {print $1" - "$3" ["$2"] ("$4")"}' | rofi -font "xos4terminus 12" -m -0 -dmenu -i -p 'Launch a livestream' )"
```

I use this script to list my Rofi scripts in Rofi:

```
#!/bin/bash
cd $HOME/Scripts/rofi; menu=$(find -L . -printf '%P\n' | sort | tail -n +2 | rofi -font "xos4terminus 12" -m -0 -bw 3 -dmenu -i -p "Rofi Scripts: "); ./"${menu}"
```

And I have bound it to `Alt`+`Shift`+`p` in i3:

```
bindsym $mod+shift+p exec "sh $HOME/Scripts/rofi-scripts.sh"
```

