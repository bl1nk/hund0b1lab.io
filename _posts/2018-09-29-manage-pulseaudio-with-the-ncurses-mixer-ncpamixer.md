---
layout: post
title: Manage Pulseaudio with the Ncurses mixer ncpamixer
tags:
  - Pulseaudio
  - ncpamixer
  - TUI
---

I wanted to record some audio the other day, but for some reason ALSA didn't want to play nice with me. It sounded like I used a tin can as a microphone across a soccer field.

After a couple of days of debugging I ended up installing Pulseaudio to see how that worked. It actually solved my issue pretty easily, even if it took some time to rebuild some of the packages in Gentoo. I went with the application [ncpamixer](https://github.com/fulhax/ncpamixer) to manage Pulseaudio, it's a fairly simple mixer with a Ncurses user-interface.

And as bonus with Pulseaudio, I can now also control the sound per application. Which is something I never missed, but once I tried it it's something I can't live without anymore.

[![A screenshot of ncpamixer with the default theme](/images/thumbnails/ncpamixer-1.png)](/images/ncpamixer-1.png)

{:.image-caption}
*Default theme*

[![A screenshot of ncpamixer with my Solarized theme](/images/thumbnails/ncpamixer-2.png)](/images/ncpamixer-2.png)

{:.image-caption}
*My theme*

The default theme didn't really look that good with my Solarized colour scheme, so I copied c0r73x's theme and made my own version that looks a lot better.

```
# solarized theme {
   "theme.solarized.default_indicator"      = "■ "
   "theme.solarized.bar_style.bg"           = "■"
   "theme.solarized.bar_style.fg"           = "■"
   "theme.solarized.bar_style.indicator"    = "■"
   "theme.solarized.bar_style.top"          = "" 
   "theme.solarized.bar_style.bottom"       = "" 
   "theme.solarized.bar_low.front"          = 0
   "theme.solarized.bar_low.back"           = -1
   "theme.solarized.bar_mid.front"          = 0
   "theme.solarized.bar_mid.back"           = -1
   "theme.solarized.bar_high.front"         = 0
   "theme.solarized.bar_high.back"          = -1
   "theme.solarized.volume_low"             = 2
   "theme.solarized.volume_mid"             = 3
   "theme.solarized.volume_high"            = 1
   "theme.solarized.volume_peak"            = 1
   "theme.solarized.volume_indicator"       = -1
   "theme.solarized.selected"               = 3
   "theme.solarized.default"                = -1
   "theme.solarized.border"                 = -1
   "theme.solarized.dropdown.selected_text" = 0
   "theme.solarized.dropdown.selected"      = 6
   "theme.solarized.dropdown.unselected"    = -1
# }
```

# Configuration

You configuration gets automatically generated and populated when you first run it and you find the file here:

```
$HOME/.config/ncpamixer.conf
```

One thing to note though; by default you can quickly change the volume in larger steps with the number keys `1-9` (`3` sets the volume to 30% et cetera), but only up to 90%, which is something I found a bit weird. The key after `9` on the number row is obviously `0`, which by default mutes the volume!?

And yes, the key `m` also mutes the volume. So why do we need the `0` key to mute the volume as well? And why wouldn't anyone want to quickly be able to set the volume to 100%? So, the first thing I did was to change the key `0` to set the volume to 100% instead:

```
"keycode.48"   = "set_volume_100"    # 0
```

# Installation

### Arch Linux

It's available in the AUR: [https://aur.archlinux.org/packages/ncpamixer-git](https://aur.archlinux.org/packages/ncpamixer-git).

### Gentoo Linux

Official ebuilds is available here: [https://github.com/fulhax/fulhax-overlay/tree/master/media-sound/ncpamixer](https://github.com/fulhax/fulhax-overlay/tree/master/media-sound/ncpamixer).
