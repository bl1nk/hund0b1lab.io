---
layout: post
title: How to use newlines and quotes with toot
tags:
  - toot
  - Mastodon
---

I post a lot of updates to Mastodon using the CLI-client [toot](https://hund0b1.gitlab.io/2018/06/17/toot-a-cli-mastodon-client.html). And one thing that can be tricky to figure out is how to post content with newlines and quotes.

For a regular post you just use toot like this:

```bash
toot post "Example content."
```

But if you want to use newlines and quotes, you need echo the content with the flag `-e`, which enables interpretation of backslash escapes.

You can then add a newline using `\n` like this:

```bash
echo -e "First line\nSecond line" | toot post
```

And if you want to use a quotes, you just need to escape the characters like this:

```bash
echo -e "\"Example quote.\"" | toot post
``` 

