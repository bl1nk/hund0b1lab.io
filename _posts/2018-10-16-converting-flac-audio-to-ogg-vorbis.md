---
layout: post
title: Converting FLAC audio to OGG Vorbis
tags:
  - FLAC
  - OGG_Vorbis
  - oggenc
  - Audio
---

I almost always buy my music from [Bandcamp](https://bandcamp.com) these days (no affiliation—just a happy customer), it's a DRM-free service that let's you download the music in various formats, like [FLAC](https://en.wikipedia.org/wiki/FLAC) and [OGG Vorbis](https://en.wikipedia.org/wiki/Vorbis).

I always download the FLAC version and then converts it to OGG Vorbis myself since I like to keep the quality a bit higher than what's really needed. And this is the reason for this post. :)

To convert FLAC music to the OGG Vorbis format you need the tool `oggenc`, which is provided with the package `vorbis-tools`. You can then convert any track with the command:
 
```
$ oggenc -q <N>
```

Where `<N>` is the quality you want. If you want to convert a whole folder you can then combine it with `find` like this:

```
$ find . -name "*flac" -exec oggenc -q 7 {} \;
```

**The quality ranges in a scale from 1 to 10.** It doesn't use explicit bit rates in its quality settings like other common formats. Partly because they don't think that you should only stare at the bit rate when measuring audio quality. And another reason for this is that OGG Vorbis uses a form of variable bit rate, where extra bits are used when the audio is harder to encode and less when the audio is simpler.

**To put the scale into perspective.** Quality `5` is near CD quality and quality `6` is enough for it to be perceived as lossless quality—unless you have an extraordinary audio setup. But if you're that deep into the rabbit hole you're probably not using anything less than CD's or FLAC anyway. :)
