---
layout: post
title: Automatically converting emojis to plain text in WeeChat
tags:
  - WeeChat
  - Emojis
---

Something I'm not a huge fan of is [emojis](https://en.wikipedia.org/wiki/Emoji), I have always preferred using the classic emoticons like `:)`. And it's not that emojis are all bad, it's just that they're not very consistent across devices, sometimes not even across applications on the same device and some applications don't even support emojis to begin with.

My terminal emulator (URxvt) supports most emojis, but they're tiny and it can be /really/ difficult trying to figure out what they're supposed to look like. Today when I was discussing emojis with a friend we got the brilliant idea that there has to be a script for WeeChat that converts emojis to plain text. And it sure does!

It's called [emoji2alias.py](https://weechat.org/scripts/source/emoji2alias.py.html/) and it converts emojis to plain text snippets like `:stuck_out_tongue:`. It's based on the script [emoji_aliases.py](https://weechat.org/scripts/source/emoji_aliases.py.html/) which lets you send emojis with aliases like `:stuck_out_tongue:`.

# Installation

You install the script in WeeChat with the command:

```
/script install emoji2alias.py
```

And if you like me don't like getting messages with text like `:stuck_out_tounge:`, then you can edit the script yourself and replace the text with `:P` or whatever you prefer. Do note that any changes will be overwritten if you update the script, so I highly recommend that you make a copy of the script, give it a unique name and use that script instead.
