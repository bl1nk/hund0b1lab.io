---
layout: post
title: Spell check and auto correction of commands in zsh
tags:
  - zsh
---

It was actually very recently when I found out about this feature, I was looking for something else and happen to stumble upon this neat feature.

Apparently zsh has the capability of spell checking and auto correcting your misspelled commands. And this is how it looks in action:

```
$ mr superSecretDatabase.db
Correct mr to rm? [Yes, No, Abort, Edit] y
```

## How to enable it

You enable the feature by adding `setopt correct` to your config `~/.zshrc` and then resourcing your configuration with the command `source ~/.zshrc`.

## Making the output more sane

By default the options will be `[nyae]` and not `[Yes, No, Abort, Edit]` as shown in my example above. You can change this by adding the following line to your configuration:

```
export SPROMPT="Correct %R to %r? [Yes, No, Abort, Edit] "
```


## Adding some colour to the output

Colours makes text easier to read and if you would like it to highlight the triggered word in red and the suggested word in green you can do so by adding this line to your configuration:
```
export SPROMPT="Correct $fg[red]%R$reset_color to $fg[green]%r$reset_color? [Yes, No, Abort, Edit] "
```

Just make sure that you have  enabled colours in zsh with `autoload -U colors && colors` for it to work.

## The one annoying drawback with the spell check

It sometimes wants to correct completely fine commands for me, like `command` to `_command`. I don't know what triggers it, but you can avoid it by creating an alias for said command:

```
alias <command>="nocorrect <command>"
```

<p style="text-align:right">
	Source: <a href="https://www.refining-linux.org/archives/39-ZSH-Gem-4-Spell-checking-and-auto-correction.html">Refining Linux — ZSH Gem #4: Spell checking and auto correction</a>
</p>
