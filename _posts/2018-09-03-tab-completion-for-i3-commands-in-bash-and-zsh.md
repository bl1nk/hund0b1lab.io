---
layout: post
title: Tab completion for i3 commands in Bash and zsh
tags:
  - i3
  - zsh
  - Bash
---

Something I miss from other window managers like [Herbstluftwm](http://herbstluftwm.org) is tab completion in the shell for the commands and features it supports. Partly because I'm lazy and partly because it's not that easy to remember every feature when you rarely use them.

With tab completion you can list all the available commands which makes it pretty easy to figure out how to do things and what's available to you. And fortunately a tab completion script for both Bash and zsh do exist, it's called [i3-completion](https://github.com/cornerman/i3-completion) and it looks something like this:

```
$ i3-msg <Tab>
[              exec           kill           nop            restart        swap
append_layout  exit           layout         open           scratchpad     title_format
bar            floating       mark           reload         shmlog         unmark
border         focus          mode           rename         split          workspace
debuglog       fullscreen     move           resize         sticky
```

# Installation

Clone the latest code from their git repository:

```
$ git clone https://github.com/cornerman/i3-completion
```

To add the script to either Bash or zsh you need to source it in you configuration (`~/.bashrc` or `~/zshrc`):

```
source "<path to file>/i3_completion.sh"
```

And if you use zsh you also need to make sure to enable the usage of Bash scripts in zsh by adding this line before sourcing the script:

```
autoload bashcompinit && bashcompinit
```

Lastly save and reload your configuration:

**Bash:**
```
source ~/.bashrc
```

**zsh:**
```
source ~/.zshrc
```

And now you should have fancy tab completions for your i3 commands. :)
