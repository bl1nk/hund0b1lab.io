---
layout: page
title: About
permalink: /about/
---

My journal is mainly about things regarding Linux, the commandline and minimalism. But I also like things like custom built mechanical keyboards and Vim, so I you might see a few posts about those things as well.

The blog itself is powered by the blog-aware, static site generator [Jekyll](https://jekyllrb.com) and it's currently hosted at [GitLab](https://about.gitlab.com). It's free from JavaScript, cookies and any other malicious features no one asked for.

# $ whoami

Hi, my name is Hund! :)

I'm a Swedish [INFJ](https://www.16personalities.com/infj-personality) dork and an avid Linux user, I'm a big advocate of both the [UNIX philosophy](https://en.wikipedia.org/wiki/Unix_philosophy), the [KISS principle](https://en.wikipedia.org/wiki/KISS_principle) and the [worse is better](https://en.wikipedia.org/wiki/Worse_is_better) concept. I consider them to be three healthy philosophical approaches to minimalist and modular software as well as life in general.

I got into Linux back in late 2006 when a friend gave me [a printed CD with Ubuntu 6.06](/images/ubuntu_6.06_cd.jpg). It didn't take long before I was completely sold on Linux and the libre software philosophy. I quickly became very active in the Swedish Ubuntu LoCo and I ended up becoming an [Ubuntu member](https://wiki.ubuntu.com/Membership) in 2009.

And as you might have figured out by now, minimalism is something that's a big part of my life. The meaning of minimalism can vary a lot depending on who you ask, but for me it's about keeping a healthy balance in life, it's about freedom and thoughtful living, and not necessarily so much about limiting yourself to a maximum amounts of items or setting your desktop wallpaper to the color `#000000` and then call it a day.


# Links

* [Mastodon](https://linuxrocks.online/@hund) - You can find me on the instance [LinuxRocks.Online](https://linuxrocks.online/@hund). It's the only social media I'm currently using.
* [deviantART](https://hundone.deviantart.com) - I'm a long time user of deviantART. I'm not as active as I used to be, but I still upload the occasional desktop screenshot and a few other things from time to time though.
* [GitLab](https://gitlab.com/users/Hund0b1/projects) - I upload all my dotfiles here. If you like something from some of my desktop screenhots you will most likely find it here. I try to keep the configuration files fairly updated.
